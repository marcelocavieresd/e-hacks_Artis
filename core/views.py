from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from core.models import *
import datetime
import json
import os
import requests
import ast
from unidecode import unidecode
from django.conf import settings
from django.template.loader import get_template
import StringIO
import os
from io import BytesIO
from django.template.loader import get_template



def login_user(request):
    template_name = "login.html"
    return render(request, template_name, {})

def landing(request):
    template_name = "landing.html"
    return render(request, template_name, {})

def my_profile(request):
    template_name = "my_profile.html"
    return render(request, template_name, {})

def index(request):
    template_name = "index.html"
    return render(request,template_name,{})