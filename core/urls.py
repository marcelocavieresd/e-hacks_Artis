from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from core import views as core_views

urlpatterns = [
    url(r'^my_profile/$', core_views.my_profile, name="my_profile"),
]
